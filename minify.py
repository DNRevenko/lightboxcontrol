import minify_html

# f = open("index.html", "r")
# minified = minify_html.minify(f.read(), minify_js=True, remove_processing_instructions=True)
# data = minified.replace("\"","\\\"")
# f = open("indexMin.html", "w")
# f.write(data)
# f.close()

import htmlmin

f = open("index.html", "r")
html = f.read()
html=htmlmin.minify(html, remove_comments=True, remove_empty_space=True)
data = html.replace("\"","\\\"")
f = open("indexMin.html", "w")
f.write(data)
f.close()
#include <NTPClient.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <ESP8266WebServer.h>
#include <EEPROM.h>

const char *ssid     = "ASUS-ASTRO";
const char *password = "248162244";

const long utcOffsetInSeconds = 0;

const int8_t dayPin = D2;
const int8_t redPin = D3;

struct interval {
  int8_t beginH = 5;
  int8_t endH = 19;
  int8_t beginM = 0;
  int8_t endM = 0;
};

struct settings {
  interval dayData;
  interval red1Data;
  interval red2Data;
  int8_t dataValid;
};

settings store;
const uint storeAddr = 0;
const int8_t dataValid = 0x3A;

int counter = 0;

// Define NTP Client to get time
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org", utcOffsetInSeconds);
ESP8266WebServer server(80);

void setDefaultSettings()
{
  store = {0};
  store.dayData.beginH = 5 - 2;
  store.dayData.endH = 19 - 2;
  store.red1Data.beginH = 4 - 2;
  store.red1Data.endH = 8 - 2;
  store.red2Data.beginH = 17 - 2;
  store.red2Data.endH = 20 - 2;
  store.dataValid = dataValid;
}

void saveSettings()
{
  EEPROM.put(storeAddr, store);
  EEPROM.commit();
}

int intervalToMinuteBegin(interval data)
{
  return data.beginH * 60 + data.beginM;
}

int intervalToMinuteEnd(interval data)
{
  return data.endH * 60 + data.endM;
}

void setup() {
  Serial.begin(115200);
  pinMode(dayPin, OUTPUT);
  pinMode(redPin, OUTPUT);
  EEPROM.begin(256);

  EEPROM.get(storeAddr, store);
  if (store.dataValid != dataValid)
  {
    setDefaultSettings();
    saveSettings();
  }


  WiFi.begin(ssid, password);

  while ( WiFi.status() != WL_CONNECTED ) {
    delay ( 500 );
    Serial.print ( "." );
  }
  Serial.println("");
  Serial.println("WiFi connected..!");
  Serial.print("Got IP: ");  Serial.println(WiFi.localIP());

  timeClient.begin();

  server.on("/", handle_OnConnect);
  server.on("/setDayData", handle_SetDayData);
  server.on("/setRed1Data", handle_SetRed1Data);
  server.on("/setRed2Data", handle_SetRed2Data);
  server.onNotFound(handle_NotFound);

  server.begin();
  Serial.println("HTTP server started");
}

void loop() {
  server.handleClient();
  if (counter >= 1000)
  {
    timeClient.update();
    int timeMinute = timeClient.getHours() * 60 + timeClient.getMinutes();
    set_dayLight(timeMinute);
    set_redLight(timeMinute);
  }
  counter ++;
  if (counter > 1000)
    counter = 0;
  delay(1);
}

void set_dayLight(int timeMinute)
{
  int beginInt = intervalToMinuteBegin(store.dayData);
  int endInt = intervalToMinuteEnd(store.dayData);

  if (timeMinute >= beginInt && timeMinute <= endInt)
    digitalWrite(dayPin, HIGH);
  else
    digitalWrite(dayPin, LOW);
}

void set_redLight(int timeMinute)
{
  int beginInt1 = intervalToMinuteBegin(store.red1Data);
  int endInt1 = intervalToMinuteEnd(store.red1Data);

  int beginInt2 = intervalToMinuteBegin(store.red2Data);
  int endInt2 = intervalToMinuteEnd(store.red2Data);

  if ((timeMinute >= beginInt1 && timeMinute <= endInt1) || (timeMinute >= beginInt2 && timeMinute <= endInt2))
    digitalWrite(redPin, HIGH);
  else
    digitalWrite(redPin, LOW);
}

String IntervalToJson(interval data)
{
  String ptr = "{";
  ptr += "\"beginH\": " + String(data.beginH) + ", ";
  ptr += "\"beginM\": " + String(data.beginM) + ", ";
  ptr += "\"endH\": " + String(data.endH) + ", ";
  ptr += "\"endM\": " + String(data.endM);
  ptr += "}";
  return ptr;
}

String SendStatus()
{
  String ptr = "{";
  ptr += "\"dayData\": " + IntervalToJson(store.dayData) + ", ";
  ptr += "\"red1Data\": " + IntervalToJson(store.red1Data) + ", ";
  ptr += "\"red2Data\": " + IntervalToJson(store.red2Data);
  ptr += "}";
  return ptr;
}

void handle_queryParamsData(interval* data)
{
  if (server.arg("beginH") != "")
  {
    (*data).beginH = server.arg("beginH").toInt();
    saveSettings();
  }
  if (server.arg("beginM") != "")
  {
    (*data).beginM = server.arg("beginM").toInt();
    saveSettings();
  }
  if (server.arg("endH") != "")
  {
    (*data).endH = server.arg("endH").toInt();
    saveSettings();
  }
  if (server.arg("endM") != "")
  {
    (*data).endM = server.arg("endM").toInt();
    saveSettings();
  }
}

void handle_SetDayData()
{
  handle_queryParamsData(&(store.dayData));
  server.send(200, "application/json", SendStatus());
}

void handle_SetRed1Data()
{
  handle_queryParamsData(&(store.red1Data));
  server.send(200, "text/plain", SendStatus());
}

void handle_SetRed2Data()
{
  handle_queryParamsData(&(store.red2Data));
  server.send(200, "text/plain", SendStatus());
}

void handle_OnConnect() {
  server.send(200, "text/html", SendHTML(0, 1, 2, 3));
}

void handle_NotFound() {
  server.send(404, "text/plain", "Not found");
}

String SendHTML(float temperature, float humidity, float pressure, float altitude) {
  String ptr;
  ptr = "<!DOCTYPE html><html lang=\"en\"><head><meta charset=\"UTF-8\"><meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"><title>Light Box</title><style>body{font-family: Arial, \"Helvetica Neue\", Helvetica, sans-serif;background-color: rgb(39, 38, 38);color: rgb(238, 235, 235);}.wrapper{margin: 50px auto;width: 320px;}header{font-size: 37px;margin-bottom: 20px;text-align: center;}.row{display: flex;flex-direction: row;justify-content: space-between;margin-bottom: 15px;}.lamp-white{color: white;margin: auto 10px auto;transform: rotateZ(40deg);}.lamp-red{color: rgb(228, 26, 26);margin: auto 10px auto;transform: rotateZ(40deg);}.time{align-self: center;}.time span{display: block;}input[type=\"time\"]::-webkit-calendar-picker-indicator{width: 20px;height: 20px;background-color: brown;}.time span input{background-color: rgb(49, 48, 48);color: rgb(238, 235, 235);margin-left: 10px;height: 25px;border-radius: 15%;border: none;}section input.end{margin-top: 5px;margin-left: 15px;}.save{margin: auto 10px auto;color: rgb(0, 162, 255);transition: all 0.2s ease;}.save:active{color: rgb(0, 4, 255);}</style></head><body><div class=\"wrapper\"><header>Light Box Manage </header><main><section class=\"row white\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"40\" height=\"40\" fill=\"currentColor\" class=\"bi bi-lightbulb-fill lamp-white\" viewBox=\"0 0 16 16\"><path d=\"M2 6a6 6 0 1 1 10.174 4.31c-.203.196-.359.4-.453.619l-.762 1.769A.5.5 0 0 1 10.5 13h-5a.5.5 0 0 1-.46-.302l-.761-1.77a1.964 1.964 0 0 0-.453-.618A5.984 5.984 0 0 1 2 6zm3 8.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1l-.224.447a1 1 0 0 1-.894.553H6.618a1 1 0 0 1-.894-.553L5.5 15a.5.5 0 0 1-.5-.5z\"/></svg><div class=\"time\"><span class=\"start\">Start<input type=\"time\" id=\"btimeId\"></span><span class=\"end\">End<input class=\"end\" type=\"time\" id=\"etimeId\"></span></div><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"40\" onclick=\"SaveData('btimeId','etimeId','setDayData')\" height=\"40\" fill=\"currentColor\" class=\"bi bi-save-fill save\" viewBox=\"0 0 16 16\"><path d=\"M8.5 1.5A1.5 1.5 0 0 1 10 0h4a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h6c-.314.418-.5.937-.5 1.5v7.793L4.854 6.646a.5.5 0 1 0-.708.708l3.5 3.5a.5.5 0 0 0 .708 0l3.5-3.5a.5.5 0 0 0-.708-.708L8.5 9.293V1.5z\"/></svg></section><section class=\"row red1\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"40\" height=\"40\" fill=\"currentColor\" class=\"bi bi-lightbulb-fill lamp-red\" viewBox=\"0 0 16 16\"><path d=\"M2 6a6 6 0 1 1 10.174 4.31c-.203.196-.359.4-.453.619l-.762 1.769A.5.5 0 0 1 10.5 13h-5a.5.5 0 0 1-.46-.302l-.761-1.77a1.964 1.964 0 0 0-.453-.618A5.984 5.984 0 0 1 2 6zm3 8.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1l-.224.447a1 1 0 0 1-.894.553H6.618a1 1 0 0 1-.894-.553L5.5 15a.5.5 0 0 1-.5-.5z\"/></svg><div class=\"time\"><span class=\"start\">Start<input type=\"time\" id=\"br1timeId\"></span><span class=\"end\">End<input class=\"end\" type=\"time\" id=\"er1timeId\"></span></div><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"40\" onclick=\"SaveData('br1timeId','er1timeId','setRed1Data')\" height=\"40\" fill=\"currentColor\" class=\"bi bi-save-fill save\" viewBox=\"0 0 16 16\"><path d=\"M8.5 1.5A1.5 1.5 0 0 1 10 0h4a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h6c-.314.418-.5.937-.5 1.5v7.793L4.854 6.646a.5.5 0 1 0-.708.708l3.5 3.5a.5.5 0 0 0 .708 0l3.5-3.5a.5.5 0 0 0-.708-.708L8.5 9.293V1.5z\"/></svg></section><section class=\"row red2\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"40\" height=\"40\" fill=\"currentColor\" class=\"bi bi-lightbulb-fill lamp-red\" viewBox=\"0 0 16 16\"><path d=\"M2 6a6 6 0 1 1 10.174 4.31c-.203.196-.359.4-.453.619l-.762 1.769A.5.5 0 0 1 10.5 13h-5a.5.5 0 0 1-.46-.302l-.761-1.77a1.964 1.964 0 0 0-.453-.618A5.984 5.984 0 0 1 2 6zm3 8.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1l-.224.447a1 1 0 0 1-.894.553H6.618a1 1 0 0 1-.894-.553L5.5 15a.5.5 0 0 1-.5-.5z\"/></svg><div class=\"time\"><span class=\"start\">Start<input type=\"time\" id=\"br2timeId\"></span><span class=\"end\">End<input class=\"end\" type=\"time\" id=\"er2timeId\"></span></div><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"40\" onclick=\"SaveData('br2timeId','er2timeId','setRed2Data')\" height=\"40\" fill=\"currentColor\" class=\"bi bi-save-fill save\" viewBox=\"0 0 16 16\"><path d=\"M8.5 1.5A1.5 1.5 0 0 1 10 0h4a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h6c-.314.418-.5.937-.5 1.5v7.793L4.854 6.646a.5.5 0 1 0-.708.708l3.5 3.5a.5.5 0 0 0 .708 0l3.5-3.5a.5.5 0 0 0-.708-.708L8.5 9.293V1.5z\"/></svg></section></main></div><script>var offset=new Date().getTimezoneOffset()/60;function getV(id){return document.getElementById(id).value;}function setV(id, v){document.getElementById(id).value=v;}function getTime(id){var data=getV(id).split(':');if(data.length < 2) return null;return{hours: data[0]*1, minutes: data[1]*1};}function pd(val){return val.toString().padStart(2,'0');}function setVV(selectB, selectE, dD){setV(selectB,`${pd(dD.beginH - offset)}:${pd(dD.beginM)}`);setV(selectE,`${pd(dD.endH - offset)}:${pd(dD.endM)}`);}async function Apply(responce){let dD=responce.dayData;let r1D=responce.red1Data;let r2D=responce.red2Data;setVV(\"btimeId\", \"etimeId\", dD);setVV(\"br1timeId\", \"er1timeId\", r1D);setVV(\"br2timeId\", \"er2timeId\", r2D);}async function SaveData(startId, endId, base){let b=getTime(startId), e=getTime(endId);if(b==null || e==null) return;let r=await fetch(`${base}?beginH=${b.hours + offset}&beginM=${b.minutes}&endH=${e.hours + offset}&endM=${e.minutes}`);Apply(await r.json());}async function Reload(){let r=await fetch(\"setDayData\");Apply(await r.json());}Reload();</script></body></html>";
  return ptr;
}
